<?php
class Roomchat extends CI_Model {
	var $table = 'chatroom';


  public function hienthi() {
		$query = $this->db->get($this->table);
		return $query->result();

	}
	public function hienthi2($room1,$room2) {
		$this->db->where('room_id1',$room1);
		$this->db->where('room_id2',$room2);
		$query = $this->db->get($this->table);
		return $query->row();
	}
	
	function insert($data) {

		if(isset($data) && count($data)) {

			if($this->db->insert($this->table,$data)) {

				return true;

			} else { return false; }

		} else { return false; }

	}

}
?>
