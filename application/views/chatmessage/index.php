<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <title>Socket.IO chat</title>
  <?php  ?>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"
  rel="stylesheet">
  <link rel="stylesheet" href="asset/css/chat.css">

</head>
  <body>
    <div class="container">
      <div
        class="row"
        style="margin-top: 20px;"
      >
          <div class="col-md-4">
            <div class="user">
              <p>List User</p>
              <?php
                if(isset($list)){
                  foreach ($list as $key => $value) {
              ?>
              <ul>
                <li>
                  <a
                    class="connect"
                    data-id="<?php echo $value->id ?>"
                    data-username="<?php echo $value->username ?>"
                  >
                    <?php echo $value->username ?>
                  </a>
                  <p class="status">status:
                      <a href="#" data-id="<?php echo $value->id ?>"  class="on-off">off</a>
                  </p>

                </li>
              </ul>

              <?php
                  }
                }
              ?>
            </div>
            <div class="useronline">

            </div>
          </div>
          <div class="cur_id">
            <a
              class="current_id"
              href="#"
              data-id="<?php print_r($this->session->userdata('user_info')->id); ?>"
            >
              <?php print_r($this->session->userdata('user_info')->username); ?>

            </a>
          </div>
          <div class="col-md-8">
              <div class="panel panel-primary">
                  <div class="panel-heading">
                      <span class="glyphicon glyphicon-comment">
                      </span> Chat
                      <div class="btn-group pull-right">
                          <button type="button"
                            class="btn btn-default btn-xs dropdown-toggle"
                            data-toggle="dropdown"
                          >
                              <span class="glyphicon glyphicon-chevron-down">
                              </span>
                          </button>
                          <ul class="dropdown-menu slidedown">
                              <li>
                                <a style="opacity: 0.4;">
                                  <span class="glyphicon glyphicon-refresh">
                                  </span>Refresh
                                </a>
                              </li>
                              <li><a style="opacity: 0.4;">
                                <span class="glyphicon glyphicon-ok-sign">
                                </span>Available
                                </a>
                              </li>
                              <li>
                                <a style="opacity: 0.4;">
                                  <span class="glyphicon glyphicon-remove">
                                  </span>Busy
                                </a>
                              </li>
                              <li>
                                <a style="opacity: 0.4;">
                                  <span class="glyphicon glyphicon-time"></span>
                                    Away
                                </a>
                              </li>
                              <li class="divider">
                              </li>
                              <li>
                                <a href="#">
                                  <span class="glyphicon glyphicon-off">
                                  </span>
                                    Leave chat
                                </a>
                              </li>
                          </ul>
                      </div>
                  </div>
                  <div class="panel-body">
                      <ul class="chat chatlist">
                      </ul>
                  </div>
                  <div class="panel-footer">
                    <form class="formmess"  method="post">
                      <div class="input-group">
                          <input
                            id="message"
                            type="text"
                            class="form-control input-sm"
                            required
                            placeholder="Type your message here..."
                            autocomplete="off"
                          >
                          <button class="btn btn-warning btn-sm"

                           id="btn-chat">
                            Send
                          </button>
                      </div>
                    </form>

                  </div>
              </div>
          </div>
      </div>
      <div class="row iframe-box" style="margin-top: 10px; display: none;">
        <div class="col-md-12 well">
            <button
              type="button"
              class="close"
              data-dismiss="iframe-box"
              aria-label="Close"
              style="top: -10px;
              position: relative;"
              onclick="closeIframe()"
            >
              <span aria-hidden="true">&times;</span>
            </button>
            <iframe height="300px;" width="100%" style="border: 0px solid;"></iframe>
        </div>
      </div>
    </div>
    <script type='text/javascript' src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <script type="text/javascript">
      //<![CDATA[
      base_url = '<?= base_url();?>';
      username = '<?= $this->session->userdata('user_info')->username ?>';
      //]]>
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/validate.js/0.12.0/validate.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/js/ajaxchat.js"></script>
  </body>
</html>
