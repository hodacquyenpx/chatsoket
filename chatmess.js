var socket = require( 'socket.io' );
var express = require( 'express' );
var http = require( 'http' );

// console.log('thi oc');

var app = express();
// console.log(app);
var server = http.createServer( app );
// console.log(server);
var io = socket.listen(server);

server.listen(8080);
var manguser = [];
io.sockets.on('connection', function( socket ) {
    socket.on("client-send-username",function(data){
      manguser.push(data);
      socket.Username = data;
      io.sockets.emit("server-send-list-user",manguser);
    });
    socket.on("disconnect", function(){
        manguser.splice(manguser.indexOf(socket.Username),1);
        io.sockets.emit("server-send-list-user",manguser);
    });
    socket.on('join_room',function(room_id){
      socket.join(room_id.roomchat);
      socket.cur_room = room_id.roomchat;
      socket.to_id = room_id.room_id1;
      socket.from_id = room_id.room_id2;
    });
    socket.on('change_room',function(room_id){
      socket.leave(socket.cur_room);
      socket.join(room_id.roomchat);
      socket.cur_room = room_id.roomchat;
      socket.to_id = room_id.room_id1;
      socket.from_id = room_id.room_id2;
    });

    socket.on('send_mesage',function(message){
      message.username = message.username;
      message.to_id = socket.to_id;
      message.from_id = socket.from_id;
      socket.broadcast.to(socket.cur_room).emit('server_send_mesage', message);
    });

});
